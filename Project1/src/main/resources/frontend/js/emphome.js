window.onload=function(){
console.log("js is linked");
getInfo();
}

let sessUser;

let tableinfo = [];

let pendingtableinfo = [];

function getSessionUser(){
	let sessuser = new XMLHttpRequest();
	sessuser.open("GET","http://localhost:9070/ersusers/sessuser");
	sessuser.onreadystatechange = function(){
		
		if(sessuser.readyState == 4 && sessuser.status >= 200 && sessuser.status < 300){
			let user = JSON.parse(sessuser.responseText);
			sessUser = user;
			userInfo(user);
			console.log(user);
		}
		
	}

	sessuser.send();
	
	
	
	function userInfo(user){
			document.getElementById('firstName').innerText=user.firstName + " " +user.lastName;
	}
	
	return sessUser;
	
}



function getPendingTable(){
	
	
	let re = new XMLHttpRequest();
	
	re.open("GET","http://localhost:9070/ersemp/pendinguser");
	
	
	re.onreadystatechange = function(){
	console.log("Status:"+re.status);
	console.log("ReadyState:"+re.readyState);
	if(re.readyState == 4 && re.status >= 200 && re.status <300){
	let tablearry = JSON.parse(re.responseText);
	tableinfo = tablearry;
	console.log(tableinfo);
	insertPendingTable();
	
	}
	}
	re.send();
	
	
	/*
	let pt = new XMLHttpRequest();
	
	pt.open("GET","http://localhost:9070/ersemp/pendinguser");
	
	pt.onreadystatechange = function(){
	console.log("readyState: "+pt.readyState);
	console.log("Status "+pt.status);
	if(pt.readyState == 4 && pt.status >= 200 && pt.status <300){
	let pendingtablearry = JSON.parse(pt.responseText);
	pendingtableinfo = pendingtablearry;
	console.log(pendingtableinfo);
	insertPendingTable();
	
	}
	}
	pt.send();

	console.log("PendingTableinfo"+pendingtableinfo)
	*/
}




function insertPendingTable(){
	console.log("inside insertPendingTable");
	let pendingtable = document.getElementById("userIdPendingTable");
	
	
	for (let i = 0; i<tableinfo.length; i++){
		let tp = "";
		let tr = "<tr>";
		tr += "<td>"+tableinfo[i].reimbId+"</td>";
		tr += "<td>"+tableinfo[i].amount+"</td>";
		tr += "<td>"+tableinfo[i].submitted+"</td>";
		tr += "<td>"+tableinfo[i].description+"</td>";
		tr += "<td>"+tableinfo[i].type+"</td>";		
		tr += "<td>"+tableinfo[i].status+"</td>";
		tp += tr;
		pendingtable.innerHTML += tp;
	}
	
}




function getReimbList(){
	
	
	let re = new XMLHttpRequest();
	
	re.open("GET","http://localhost:9070/ersemp/pastuserreimb");
	
	
	re.onreadystatechange = function(){
	if(re.readyState == 4 && re.status >= 200 && re.status <300){
	let tablearry = JSON.parse(re.responseText);
	tableinfo = tablearry;
	console.log(tableinfo);
	insertReimbList();
	
	}
	}
	re.send();

	console.log("Tableinfo"+tableinfo)

}




function insertReimbList(){
	console.log("inside insertReimbList");
	let table = document.getElementById("userIdReimbTable");
	
	
	for (let i = 0; i<tableinfo.length; i++){
		let t = "";
		let tr = "<tr>";
		tr += "<td>"+tableinfo[i].reimbId+"</td>";
		tr += "<td>"+tableinfo[i].amount+"</td>";
		tr += "<td>"+tableinfo[i].submitted+"</td>";
		tr += "<td>"+tableinfo[i].resolved+"</td>";
		tr += "<td>"+tableinfo[i].description+"</td>";
		tr += "<td>"+tableinfo[i].firstName+" "+tableinfo[i].lastName+"</td>";
		tr += "<td>"+tableinfo[i].type+"</td>";
		tr += "<td>"+tableinfo[i].status+"</td>";
		t += tr;
		table.innerHTML += t;
	}
	
}


async function getInfo(){
	try{
		let response = await getSessionUser();
		getPendingTable(response);
		getReimbList(response);
		
	}
	catch(error){
		console.log(error);
	}

}


