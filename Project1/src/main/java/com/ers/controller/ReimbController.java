package com.ers.controller;

import java.time.LocalDate;

//import com.ers.model.Reimb;
import com.ers.model.User;
import com.ers.service.ReimbService;
//import com.ers.service.UserService;


import io.javalin.http.Handler;

public class ReimbController {
	
	private ReimbService rServ = new ReimbService();
//	private static UserService uServ = new UserService();
	
		
	public ReimbController() {
		
	}

	public ReimbController(ReimbService rServ) {
		super();
		this.rServ = rServ;
	}
	
	
	public final Handler NEWREIMB = (ctx) ->{
		System.out.println("In new reimb");
		
		User sessuser = (User)ctx.sessionAttribute("currentuser");
		System.out.println("Got User "+sessuser);
		
		
		int authorId = sessuser.getUserId();
		double amount = Double.parseDouble(ctx.formParam("amount"));
		int typeId = rServ.convertTypeId(ctx.formParam("typeId"));
		LocalDate submitdate = LocalDate.parse(ctx.formParam("submitdate"));
		String desc = ctx.formParam("description");
		rServ.insertReimb(amount, submitdate, desc, authorId, typeId);
		ctx.status(200);
		ctx.redirect("/html/emphome.html");
		
	};
	
	public final Handler ALTERREIMB = (ctx) ->{
		
		User sessuser = (User)ctx.sessionAttribute("currentuser");
		System.out.println("Inside Alterreimb");
		int reid = Integer.parseInt(ctx.formParam("reimbId"));
		LocalDate resolvedate = LocalDate.parse(ctx.formParam("resolvedate"));
		int resolver = sessuser.getUserId();
		int statusId = Integer.parseInt(ctx.formParam("statusId"));
		rServ.alterReimb(reid, resolvedate, resolver, statusId);
		ctx.status(200);
		ctx.redirect("/html/manhome.html");
	};
	
	
	public final Handler PASTREIMBUSER = (ctx) ->{
		
		User sessuser = (User)ctx.sessionAttribute("currentuser");
		System.out.println("Got User in PASTREIMBUSER");
		int authId = sessuser.getUserId();
		ctx.status(200);
		ctx.json(rServ.getUserReimbs(authId));
	};
	
	public final Handler PENDINGREIMBUSER = (ctx) ->{
		
		User sessuser = (User)ctx.sessionAttribute("currentuser");
		System.out.println("Got User in PENDINGREIMBUSER");
		int authId = sessuser.getUserId();
		ctx.status(200);
		ctx.json(rServ.getPendingReimbs(authId));
	};
	
	public final Handler ALLREIMBS = (ctx) ->{
		ctx.status(200);
		ctx.json(rServ.getAllReimbs());
	};
	
	public final Handler PENDINGREIMBMAN = (ctx) ->{
		ctx.status(200);
		ctx.json(rServ.getManPendingReimbs());
	};
	
	public final Handler AMOUNTLIST = (ctx) ->{
		ctx.status(200);
		ctx.json(rServ.getSumPerUser());
	};
	
	public final Handler TYPELIST = (ctx) ->{
		ctx.status(200);
		ctx.json(rServ.getSumPerType());
	};
	
	
	
	
	

}
