package com.ers.controller;

import com.ers.model.User;
import com.ers.service.UserService;


import io.javalin.http.Handler;

public class UserController {
	
	private UserService uServ = new UserService();
	
	
	public UserController() {
	}


	public UserController(UserService uServ) {
		super();
		this.uServ = uServ;
	}
	
	public final Handler LOGIN = (ctx) -> {
		User user = uServ.verifyPassword(ctx.formParam("username"), ctx.formParam("password"));
		String role = uServ.verifyRole(user.getRoleId());
		
		
		if(role.equals("employee")) {
			ctx.sessionAttribute("currentuser", user);
			ctx.redirect("/html/emphome.html");
		}
		else if(role.equals("manager")) {
			ctx.sessionAttribute("currentuser", user);
			ctx.redirect("/html/manhome.html");
		}
		else {
			ctx.redirect("/html/badlogin.html");
		}
	};
	
	
	public final Handler GETSESSUSER = (ctx) ->{

		User sessuser = (User)ctx.sessionAttribute("currentuser");
		System.out.println(sessuser);
		ctx.status(200);
		ctx.json(sessuser);
		
	};
	
	
	

}
