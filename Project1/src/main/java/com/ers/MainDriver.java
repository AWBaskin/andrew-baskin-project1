package com.ers;

import java.io.File;
//import java.time.LocalDate;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import com.ers.controller.ReimbController;
import com.ers.controller.UserController;
import com.ers.dao.ERSDBConnection;
import com.ers.dao.ReimbDAOImpl;
import com.ers.dao.UserDAOImpl;
import com.ers.service.ReimbService;
import com.ers.service.UserService;

import io.javalin.Javalin;

public class MainDriver {

	public static void main(String[] args) {
		
		
		
		UserController uCon = new UserController(new UserService(new UserDAOImpl( new ERSDBConnection())));
		ReimbController rCon = new ReimbController(new ReimbService(new ReimbDAOImpl( new ERSDBConnection())));
//		UserDAOImpl udao = new UserDAOImpl(new ERSDBConnection());
//		UserService userv = new UserService(new UserDAOImpl( new ERSDBConnection()));
//		ReimbService rserv = new ReimbService(new ReimbDAOImpl(new ERSDBConnection()));
//		ReimbDAOImpl rdao = new ReimbDAOImpl(new ERSDBConnection());


//		rdao.adjustReimb(6, null, 1, 2);
//		rdao.newReimb(555, LocalDate.of(2222, 01, 22), "testing in driver", 2, 1);
//		rserv.alterReimb(6, LocalDate.of(2222, 1, 22), 1, 2);
		
//		rserv.insertReimb(300, LocalDate.of(2222, 1, 22), "From Driver", 2, 1);


//		System.out.println(rserv.getManPendingReimbs());
//		System.out.println(rserv.getPendingReimbs(2));
//		System.out.println(rserv.getUserReimbs(2));
		

		
		
		
		Javalin app = Javalin.create(config->{
			config.enableCorsForAllOrigins();
			config.addStaticFiles("/frontend");
		});
		
		
		app.start(9070);
		
		app.post("/ersusers/login", uCon.LOGIN);
		
		app.post("/ersemp/reimbsubmit", rCon.NEWREIMB);
		
		app.get("/ersusers/sessuser", uCon.GETSESSUSER);
		
		app.post("/ersman/reimbalter", rCon.ALTERREIMB);
		
		app.get("/ersemp/pastuserreimb", rCon.PASTREIMBUSER);
		
		app.get("/ersemp/pendinguser", rCon.PENDINGREIMBUSER);
		
		app.get("/ersman/allreimb", rCon.ALLREIMBS);
		
		app.get("/ersman/pendingreimb", rCon.PENDINGREIMBMAN);
		
		app.get("/amountlist", rCon.AMOUNTLIST);
		
		app.get("/typelist", rCon.TYPELIST);
		
		
		
		app.exception(NullPointerException.class, (e, ctx)->{
			System.out.println("Null Pointer Exception");
			ctx.status(404);
			ctx.redirect("/html/badlogin.html");
		});
		
		File file = new File("src/main/resources/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());

        WebDriver driver = new ChromeDriver();

        driver.get("http://localhost:9070/html/index.html");
		
	}

}
