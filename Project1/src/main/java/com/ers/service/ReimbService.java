package com.ers.service;

import java.time.LocalDate;
import java.util.List;

import com.ers.dao.ReimbDAOImpl;
//import com.ers.model.Reimb;
import com.ers.model.Reimb;

public class ReimbService {
	
	private ReimbDAOImpl rDao;
	
	public ReimbService() {
	}

	public ReimbService(ReimbDAOImpl rDao) {
		super();
		this.rDao = rDao;
	}
	
	
	public void insertReimb(double amount, LocalDate submitted, String description, int authorId, int typeId) {
		rDao.newReimb(amount, submitted, description, authorId, typeId);
		
	}
	
	
	
	public Integer convertTypeId(String type) {
		int typeId = 1;
		if(type.equals("travel")) {
			typeId = 1;
		}
		else if(type.equals("gas")) {
			typeId = 2;
		}
		else if(type.equals("food")) {
			typeId = 3;
		}
		return typeId;
	}
	
	public void alterReimb(int reimbId, LocalDate resolved, int resolver, int statusId) {
		rDao.adjustReimb(reimbId, resolved, resolver, statusId);
	}
	
	
	public List<Reimb> getUserReimbs(int authorId){
		List<Reimb>reimbList = rDao.allReimbForUser(authorId);
		return reimbList;
	}
	
	public List<Reimb> getPendingReimbs(int authorId){
		List<Reimb>reimbList = rDao.pendingReimbs(authorId);
		return reimbList;
	}
	
	public List<Reimb> getAllReimbs(){
		List<Reimb>reimbList = rDao.allReimbsMan();
		return reimbList;
	}
	
	public List<Reimb> getManPendingReimbs(){
		List<Reimb>reimbList = rDao.pendingReimbsMan();
		return reimbList;
	}
	
	public List<Reimb> getSumPerUser(){
		List<Reimb>amountList = rDao.amountPerUser();
		return amountList;
	}
	
	public List<Reimb> getSumPerType(){
		List<Reimb>typeList = rDao.amountPerType();
		return typeList;
	}
	
	

}
