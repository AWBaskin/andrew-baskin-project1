package com.ers.service;

import com.ers.dao.UserDAOImpl;
import com.ers.model.User;

public class UserService {
	
	private UserDAOImpl uDao;
	
	public UserService() {
	}

	public UserService(UserDAOImpl uDao) {
		super();
		this.uDao = uDao;
	}
	
	public User findUser(String username) {
		User user = uDao.getUser(username);
		if(user == null) {
			throw new NullPointerException();
		}
		return user;
	}
	
	public User verifyPassword(String username, String password) {
		User user = findUser(username);
		if(user.getPassword().equals(password)) {
			return user;
		}
		return null;
	}
	
	public String verifyRole(int roleId) {
		String role = uDao.getRole(roleId);
		return role;
	}
	

}
