package com.ers.dao;

import java.time.LocalDate;


import java.util.List;

import com.ers.model.Reimb;




public interface ReimbDAO {

	void newReimb(double amount, LocalDate submitted, String description, int authorId, int typeId);
	void adjustReimb(int reimbId, LocalDate resolved, int resolverId, int statusId);
	List<Reimb> allReimbForUser(int userId);
	List<Reimb> pendingReimbs(int userId);
	List<Reimb> pendingReimbsMan();
	List<Reimb> allReimbsMan();
	List<Reimb> amountPerUser();
	List<Reimb> amountPerType();
	

}
