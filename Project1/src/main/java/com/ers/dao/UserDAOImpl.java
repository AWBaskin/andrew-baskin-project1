package com.ers.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ers.model.User;

public class UserDAOImpl implements UserDAO{
	
	private ERSDBConnection edc;
	
	
	
	public UserDAOImpl(ERSDBConnection edc) {
		super();
		this.edc = edc;
	}

	public UserDAOImpl() {
	}



	@Override
	public User getUser(String username){
		
		try(Connection con = edc.getDBConnection()){
			String sql = "select * from ers_users where ers_username = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1,  username);
			ResultSet rs = ps.executeQuery();
			User user = new User();
			while(rs.next()) {
				user = new User(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getInt(7));
			}
			return user;
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public String getRole(int roleId) {
		
		String role = new String();

		try(Connection con = edc.getDBConnection()){
			String sql = "select user_role from ers_user_roles where ers_user_role_id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, roleId);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				role = rs.getString(1);
			}
			
			return role;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
