package com.ers.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ERSDBConnection {
	
	private static final String URL = "jdbc:postgresql://awb-rev-db.cohdzwvqac6y.us-east-2.rds.amazonaws.com:5432/ersdb";
	private static final String username = "ersuser";
	private static final String password = "P4ssw0rd";
	
	public Connection getDBConnection() throws SQLException{
		return DriverManager.getConnection(URL, username, password);
	}

}
