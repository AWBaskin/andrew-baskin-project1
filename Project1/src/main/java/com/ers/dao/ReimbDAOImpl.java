package com.ers.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


import com.ers.model.Reimb;




public class ReimbDAOImpl implements ReimbDAO{
	

	
	private ERSDBConnection edc;
	
	public ReimbDAOImpl() {
	}
	

	public ReimbDAOImpl(ERSDBConnection edc) {
		super();
		this.edc = edc;
	}




	@Override
	public void newReimb(double amount, LocalDate submitted, String description, int authorId, int typeId) {

		try(Connection con = edc.getDBConnection()){
			String sql = "{? = call new_reimbursement(?,?,?,?,?)}";
			CallableStatement cs = con.prepareCall(sql);
			cs.registerOutParameter(1, Types.VARCHAR);
			cs.setBigDecimal(2, BigDecimal.valueOf(amount));
			cs.setObject(3, submitted);
			cs.setString(4, description);
			cs.setInt(5, authorId);
			cs.setInt(6, typeId);
			cs.execute();
			System.out.println(cs.getString(1));
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void adjustReimb(int reimbId, LocalDate resolved, int resolverId, int statusId) {
		System.out.println("inside adjust reimb");
		try(Connection con = edc.getDBConnection()){
			System.out.println("inside try func");
			String sql = "{? = call edit_reimbursement(?,?,?,?)}";
			CallableStatement cs = con.prepareCall(sql);
			cs.registerOutParameter(1, Types.VARCHAR);
			cs.setInt(2, reimbId);
			cs.setObject(3, resolved);
			cs.setInt(4, resolverId);
			cs.setInt(5, statusId);
			cs.execute();
			System.out.println(cs.getString(1));
		}catch(SQLException e) {
			e.getStackTrace();
		}
	}
	
	

	@Override
	public List<Reimb> allReimbForUser(int userId) {
		
		
		List<Reimb> reimbList = new ArrayList<>();
		
		try(Connection con = edc.getDBConnection()){
//			String sql = "select reimb_id, reimb_amount, reimb_submitted, reimb_description, reimb_status_id from ers_reimbursement where reimb_author = ? and status_id > 1";
//			PreparedStatement ps = con.prepareCall(sql);
//			ps.setInt(1, userId);
			
			String sql = "select r.reimb_id, r.reimb_amount, r.reimb_submitted, r.reimb_resolved, r.reimb_description, u.user_first_name, u.user_last_name, t.reimb_type, s.reimb_status from ers_reimbursement r join ers_users u on r.reimb_resolver = u.ers_user_id join ers_reimbursement_type t on r.reimb_type_id = t.reimb_type_id join ers_reimbursement_status s on r.reimb_status_id = s.reimb_status_id where r.reimb_status_id >1 and reimb_author = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				reimbList.add(new Reimb(rs.getInt(1), rs.getDouble(2), rs.getObject(3, LocalDate.class), rs.getObject(4, LocalDate.class), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
			}
			System.out.println("allReimbForUser" + reimbList);
			return reimbList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	





	@Override
	public List<Reimb> pendingReimbs(int userId) {
		List<Reimb> reimbList = new ArrayList<>();
		
		try(Connection con = edc.getDBConnection()){
//			String sql = "select reimb_id, reimb_amount, reimb_submitted, reimb_description, reimb_status_id from ers_reimbursement where reimb_author = ? and status_id = 1";
//			PreparedStatement ps = con.prepareCall(sql);
//			ps.setInt(1, userId);
//			ResultSet rs = ps.executeQuery();
//			
//			while(rs.next()) {
//				reimbList.add(new Reimb(rs.getInt(1), rs.getDouble(2), rs.getObject(3, LocalDate.class), rs.getString(4), rs.getInt(5)));
//			}
//			return reimbList;
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}return null;
		
		String sql = "select r.reimb_id, u.user_first_name, u.user_last_name, r.reimb_amount, r.reimb_submitted, r.reimb_resolved, r.reimb_description, t.reimb_type, s.reimb_status from ers_reimbursement r join ers_reimbursement_type t on r.reimb_type_id = t.reimb_type_id join ers_users u on r.reimb_author = u.ers_user_id join ers_reimbursement_status s on r.reimb_status_id = s.reimb_status_id where r.reimb_status_id = 1 and reimb_author = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setInt(1, userId);
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()) {
			reimbList.add(new Reimb(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDouble(4), rs.getObject(5, LocalDate.class), rs.getObject(6, LocalDate.class), rs.getString(7), rs.getString(8), rs.getString(9)));
		}
		System.out.println("pendingReimbs" + reimbList);
		return reimbList;
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return null;
	}


	@Override
	public List<Reimb> pendingReimbsMan() {
		List<Reimb> reimbList = new ArrayList<>();
		
		try(Connection con = edc.getDBConnection()){
//			String sql = "select reimb_id, reimb_amount, reimb_submitted, reimb_description, reimb_status_id from ers_reimbursement where status_id > 1";
//			PreparedStatement ps = con.prepareCall(sql);
//
//			ResultSet rs = ps.executeQuery();
//			
//			while(rs.next()) {
//				reimbList.add(new Reimb(rs.getInt(1), rs.getDouble(2), rs.getObject(3, LocalDate.class), rs.getString(4), rs.getInt(5)));
//			}
//			return reimbList;
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return null;
			
			String sql = "select r.reimb_id, u.user_first_name, u.user_last_name, r.reimb_amount, r.reimb_submitted, r.reimb_resolved, r.reimb_description, t.reimb_type, s.reimb_status from ers_reimbursement r	join ers_users u on r.reimb_author = u.ers_user_id join ers_reimbursement_type t on r.reimb_type_id = t.reimb_type_id join ers_reimbursement_status s on r.reimb_status_id = s.reimb_status_id where r.reimb_status_id = 1";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				reimbList.add(new Reimb(rs.getInt(1),rs.getString(2), rs.getString(3), rs.getDouble(4), rs.getObject(5, LocalDate.class), rs.getObject(6, LocalDate.class), rs.getString(7), rs.getString(8), rs.getString(9)));
			}
			return reimbList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public List<Reimb> allReimbsMan() {
		List<Reimb> reimbList = new ArrayList<>();
		
		try(Connection con = edc.getDBConnection()){
//			String sql = "select reimb_id, reimb_amount, reimb_submitted, reimb_description, reimb_status_id from ers_reimbursement where status_id = 1";
//			PreparedStatement ps = con.prepareCall(sql);
//			ResultSet rs = ps.executeQuery();
//			
//			while(rs.next()) {
//				reimbList.add(new Reimb(rs.getInt(1), rs.getDouble(2), rs.getObject(3, LocalDate.class), rs.getString(4), rs.getInt(5)));
//			}
//			return reimbList;
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return null;
			
			String sql = "select r.reimb_id, u.user_first_name, u.user_last_name, r.reimb_amount, r.reimb_submitted, r.reimb_resolved, r.reimb_description, t.reimb_type, s.reimb_status from ers_reimbursement r join ers_users u on r.reimb_author = u.ers_user_id join ers_reimbursement_type t on r.reimb_type_id = t.reimb_type_id join ers_reimbursement_status s on r.reimb_status_id = s.reimb_status_id where r.reimb_status_id >1";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				reimbList.add(new Reimb(rs.getInt(1),rs.getString(2), rs.getString(3), rs.getDouble(4), rs.getObject(5, LocalDate.class), rs.getObject(6, LocalDate.class), rs.getString(7), rs.getString(8), rs.getString(9)));
			}
			return reimbList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public List<Reimb> amountPerUser() {
		
		List<Reimb> amountList = new ArrayList<>();
		
		try(Connection con = edc.getDBConnection()){
			
			String sql = "select eu.user_first_name, eu.user_last_name, sum(er.reimb_amount) from ers_reimbursement er "
					+ "join ers_users eu on eu.ers_user_id = er.reimb_author "
					+ "join ers_reimbursement_type ert on ert.reimb_type_id = er.reimb_type_id  "
					+ "group by er.reimb_author, eu.user_first_name, eu.user_last_name";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				amountList.add(new Reimb(rs.getString(1), rs.getString(2), rs.getDouble(3)));
			}
			return amountList;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return null;
	}


	@Override
	public List<Reimb> amountPerType() {
		List<Reimb> typeList = new ArrayList<>();
		
		try(Connection con = edc.getDBConnection()){
			
			String sql = "select ert.reimb_type, sum(er.reimb_amount) from ers_reimbursement er "
					+"join ers_reimbursement_type ert on ert.reimb_type_id = er.reimb_type_id "
					+"group by ert.reimb_type";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				typeList.add(new Reimb(rs.getString(1), rs.getDouble(2)));
			}
			return typeList;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}




