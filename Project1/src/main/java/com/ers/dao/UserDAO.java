package com.ers.dao;

import com.ers.model.User;

public interface UserDAO {
	
	String getRole(int roleId);
	User getUser(String username);
	

}
