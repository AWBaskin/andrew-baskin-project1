package com.ers.runners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
			features = {"feature/EmployeeLogin.feature", "feature/ManagerLogin.feature", "feature/ReimbRequest.feature", "feature/ReimbStatus.feature"},
			glue = {"com.ers.gluecode"}
		)


public class ErsTestRunner {

}


