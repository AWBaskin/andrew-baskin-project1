package com.ers.gluecode;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ers.eval.ErsTesting;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ErsManLoginTest {
	
	public ErsTesting el;
	public String username;
	public String password;
	
	@Given("a manager is at the home page")
	public void a_manager_is_at_the_home_page() {
		this.el = new ErsTesting(ErsDriverUtility.driver);
		assertEquals("ERS Login", ErsDriverUtility.driver.getTitle());
	}
	
	@When("a user of role manager inputs their username {string}")
	public void a_user_of_role_manager_inputs_their_username(String string) {
		this.username = string;
	}
	
	@When("a user of role manager inputs their password {string}")
	public void a_user_of_role_manager_inputs_their_password(String string) {
		this.password = string;
	}
	
	@When("a user submits their manager information")
	public void a_user_submits_their_manager_information() {
		el.ersLogin(this.username, this.password);
	}
	
	@Then("the manager is redirected to the manager ers page")
	public void the_user_is_redirected_to_the_manager_ers_page() {
		WebDriverWait wait = new WebDriverWait(ErsDriverUtility.driver, 5);
		wait.until(ExpectedConditions.titleContains("Manager Home Page"));
		assertEquals("Welcome Manager", ErsDriverUtility.driver.findElement(By.tagName("h1")).getText());
	}

}
