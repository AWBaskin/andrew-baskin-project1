package com.ers.gluecode;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;

public class ErsDriverUtility {
	
	public static WebDriver driver;
	
	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://localhost:9070/html/index.html");
	}
	

	@After
	public void tearDown() {
		if(driver !=null) {
			driver.quit();
		}
	}

}
