package com.ers.gluecode;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//import java.time.LocalDate;

import com.ers.eval.ErsTesting;
//import com.fasterxml.jackson.annotation.JsonFormat;
//import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
//import com.fasterxml.jackson.databind.annotation.JsonSerialize;
//import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
//import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ReimbRequestTest {
	public ErsTesting et;
	

	public String amount;
	public String description;
//	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
//	@JsonDeserialize(using = LocalDateDeserializer.class)
//	@JsonSerialize(using = LocalDateSerializer.class)
	public String submitDate;
	public String username;
	public String password;
	
	@Given ("an employee is logged into the employee webpage {string}") 
		
		public void an_employee_is_logged_into_the_employee_webpage(String string) {
			this.et = new ErsTesting(ErsDriverUtility.driver);
			this.username = string;
		}
	
	@When ("an an employee inputs their password {string}")
		
		public void an_employee_inputs_their_password(String string) {
		this.password = string;
	}
		
	@When ("an employee inputs the amount {string}")
		public void an_employee_inputs_the_amount(String string) {
		this.amount = string;
	}
	
	@When ("an employee inputs the description {string}")
		public void an_employee_inputs_the_description(String string) {
		this.description = string;
	}
	@When ("an employee inputs the submitDate {string}")
		public void an_employee_inputs_the_submitDate(String string) {
		this.submitDate = string;
	}
	
	
//	@When ("a user inputs the information for a reimbursement {amount}, {description}, {submitDate}") 
//		public void a_user_inputs_the_information_for_a_reimbursement(String amount, String description, String submitDate) {
//			this.amount = amount;
//			this.description = description;
//			this.submitDate = submitDate;
//		}
	
	
	
	@Then ("the reimbursement is submitted") 
		public void the_reimbursement_is_submitted() {
		et.ersEmpReimbSubmit(this.username, this.password, this.description, this.submitDate, this.amount);
			
		}
	

}
