package com.ers.gluecode;

import static org.junit.Assert.assertEquals;

//import java.time.LocalDate;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ers.eval.ErsTesting;
//import com.fasterxml.jackson.annotation.JsonFormat;
//import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
//import com.fasterxml.jackson.databind.annotation.JsonSerialize;
//import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
//import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ReimbStatusTest {
	
	public ErsTesting et;
	
	public String username;
	public String password;
	private String reimbId;
//	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
//	@JsonDeserialize(using = LocalDateDeserializer.class)
//	@JsonSerialize(using = LocalDateSerializer.class)
	private String resolveDate;
//	private String status;
	
	@Given ("a manager is logged into the manager webpage {string}")
		public void a_manager_is_logged_into_the_manager_webpage(String string) {
		
		this.et = new ErsTesting(ErsDriverUtility.driver);
		this.username = string;
		}
	
	@When ("a manager password {string}")
		public void a_manager_password(String string) {
		this.password = string;
//		et.ersLogin(this.username, this.password);
//		WebDriverWait wait = new WebDriverWait(ErsDriverUtility.driver, 5);
//		wait.until(ExpectedConditions.titleContains("Manager Home Page"));
//		assertEquals("Welcome Manager", ErsDriverUtility.driver.findElement(By.tagName("h1")).getText());
	}
	
	@When ("a manager can view all reimbursement requests")
		public void a_manager_can_view_all_reimbursement_requests() {
		assertEquals("Past Reimbursements", ErsDriverUtility.driver.findElement(By.id("Past Reimbursements")).getText());
		
	}
	
	@When ("that manager can approve or deny a reimbursement request by inputing the resolveDate {string}")
		public void that_manager_can_approve_or_deny_a_reimbursement_request_by_inputing_the_resolveDate(String string) {
		this.resolveDate = string;
	}
	
	@When ("that manager inputs the reimbId {string}")
		public void that_manager_inputs_the_reimbId(String string) {
		this.reimbId = string;
	}
	
	@Then ("the reimbursement status is submitted")
		public void the_reimbursement_status_is_submitted() {
		et.ersManReimbApprove(this.username, this.password, this.reimbId, this.resolveDate);
		
	}

	


}
