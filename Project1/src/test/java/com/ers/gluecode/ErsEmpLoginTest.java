package com.ers.gluecode;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ers.eval.ErsTesting;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ErsEmpLoginTest {
	
	public ErsTesting et;
	public String username;
	public String password;
	
	@Given("an employee is at the home page")
	public void an_employee_is_at_the_home_page() {
		this.et = new ErsTesting(ErsDriverUtility.driver);
		assertEquals("ERS Login", ErsDriverUtility.driver.getTitle());
	}
	
	@When("a user of role employee inputs their username {string}")
	public void a_user_of_role_employee_inputs_their_username(String string) {
		this.username = string;
	}
	
	@When("a user of role employee inputs their password {string}")
	public void a_user_of_role_employee_inputs_their_password(String string) {
		this.password = string;
	}
	
	@When("a user submits their employee information")
	public void a_user_submits_their_employee_information() {
		et.ersLogin(this.username, this.password);
	}
	
	@Then("the user is redirected to the employee ers page")
	public void the_user_is_redirected_to_the_employee_ers_page() {
		WebDriverWait wait = new WebDriverWait(ErsDriverUtility.driver, 5);
		wait.until(ExpectedConditions.titleContains("Employee Home Page"));
		assertEquals("Welcome", ErsDriverUtility.driver.findElement(By.tagName("h1")).getText());
	}
	
	

}
