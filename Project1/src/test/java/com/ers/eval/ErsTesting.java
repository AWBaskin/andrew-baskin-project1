package com.ers.eval;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
public class ErsTesting {


	
	@FindBy(name="username")
	private WebElement username;
	
	
	@FindBy(name="password")
	private WebElement password;
	
	@FindBy(name="usersubmit")
	private WebElement submitButton;
	
	@FindBy(name="amount")
	private WebElement amount;
	
	@FindBy(name="submitdate")
	private WebElement submitDate;
	
	@FindBy(name="description")
	private WebElement description;
	
	@FindBy(name="altersubmit")
	private WebElement altersubmit;
	
	@FindBy(name="resolvedate")
	private WebElement resolveDate;
	
	@FindBy(name="reimbId")
	private WebElement reimbId;
	
	@FindBy(name="subbutton")
	private WebElement reimbsubmit;
	
//	private WebDriver driver;
//	private WebElement header;
//	private WebElement passwordField;
//	private WebElement usernameField;
//	private WebElement submitButton;
//	
	
	public ErsTesting(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void ersLogin(String username, String password) {

		this.username.clear();
		this.password.clear();
		this.password.sendKeys(password);
		this.username.sendKeys(username);
		this.submitButton.click();
	}
	
	public void ersEmpReimbSubmit(String username, String password, String description, String submitDate, String amount) {
		
		
		this.username.clear();
		this.password.clear();
		this.password.sendKeys(password);
		this.username.sendKeys(username);
		this.submitButton.click();
		this.amount.clear();
		this.description.clear();
		this.submitDate.clear();
		this.amount.sendKeys(amount);
		this.submitDate.sendKeys(submitDate);
		this.description.sendKeys(description);
		this.reimbsubmit.click();
		
		
	}
	
	public void ersManReimbApprove(String username, String password, String reimbId, String resolveDate) {
		
		this.username.clear();
		this.password.clear();
		this.password.sendKeys(password);
		this.username.sendKeys(username);
		this.submitButton.click();
		
		this.reimbId.clear();
		this.resolveDate.clear();
		this.reimbId.sendKeys(reimbId);
		this.resolveDate.sendKeys(resolveDate);		
		this.altersubmit.click();
	}
//	
//	public void navigateTo() {
//		this.driver.get("http://localhost:9070/html/index.html");
//	}
	
	

}
