package com.ers.eval;



//import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ers.dao.ERSDBConnection;
import com.ers.dao.ReimbDAOImpl;
import com.ers.model.Reimb;


public class ReimbDAOTest {
	

	@Mock
	private ERSDBConnection edb;
	
	@Mock
	private Connection con;
	
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private CallableStatement cs;
	
	@Mock
	private ResultSet rs;
	
	private ReimbDAOImpl rDao;
	
	private Reimb reimb;
	private List<Reimb> reimbList;

	
	
	@BeforeEach
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		this.rDao = new ReimbDAOImpl(edb);
		when(edb.getDBConnection()).thenReturn(con);
		when(con.prepareStatement(isA(String.class))).thenReturn(ps);
		when(con.prepareCall(isA(String.class))).thenReturn(cs);
		when(ps.executeQuery()).thenReturn(rs);
		when(ps.execute()).thenReturn(true);
		when(cs.execute()).thenReturn(true);
		when(cs.getString(isA(Integer.class))).thenReturn("Test Success");
		reimb = new Reimb(1, "Bandrew", "Baskin", 23, LocalDate.of(2222, 01, 22), LocalDate.of(2222, 01, 22), "duh", "travel", "approved");
		reimbList = Arrays.asList(reimb);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt(1)).thenReturn(reimb.getReimbId());
		when(rs.getString(2)).thenReturn(reimb.getFirstName());
		when(rs.getString(3)).thenReturn(reimb.getLastName());
		when(rs.getDouble(4)).thenReturn(reimb.getAmount());
		when(rs.getObject(5)).thenReturn(reimb.getSubmitted());
		when(rs.getObject(6)).thenReturn(reimb.getResolved());
		when(rs.getString(7)).thenReturn(reimb.getDescription());
		when(rs.getString(8)).thenReturn(reimb.getType());
		when(rs.getString(9)).thenReturn(reimb.getStatus());
		
	}

	
	@Test
	public void newReimbSuccess() throws SQLException{
		rDao.newReimb(34, LocalDate.of(2222, 02, 02), "dut", 2, 1);
		verify(cs, times(1)).execute();
	}
	
	@Test
	public void adjustReimbSuccess() throws SQLException{
		rDao.adjustReimb(1, LocalDate.of(2222, 02, 02), 1, 2);
		verify(cs, times(1)).execute();
	}
	
//	@Test
//	public void allReimbsForUserSuccess() throws SQLException{
//		rDao.allReimbForUser(1);
//		verify(ps, times(1)).execute();
//	}
//	
//	@Test
//	public void pendingReimbsSuccess() throws SQLException{
//		rDao.pendingReimbs(2);
//		verify(ps, times(1)).execute();
//	}
//	
//	@Test
//	public void pendingReimbsManSuccess() throws SQLException{
//		rDao.pendingReimbsMan();
//		verify(ps, times(1)).execute();
//	}
//	
//	@Test
//	public void allReimbsManSuccess() throws SQLException{
//		rDao.allReimbsMan();
//		verify(ps, times(1)).execute();
//	}
	
//	@Test
//	public void amountPerUserSuccess() throws SQLException{
//		rDao.amountPerUser();
//		verify(ps, times(1)).execute();
//	}
//	
//	@Test
//	public void amountPerTypeSuccess() throws SQLException{
//		rDao.amountPerType();
//		verify(ps, times(1)).execute();
//	}
	
	

}
