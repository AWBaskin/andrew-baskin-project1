package com.ers.eval;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import com.ers.dao.ERSDBConnection;
import com.ers.dao.UserDAOImpl;
import com.ers.model.User;

public class UserDAOTest {
	
	
	@Mock
	private ERSDBConnection edb;
	
	@Mock
	private Connection con;
	
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private CallableStatement cs;
	
	@Mock
	private ResultSet rs;
	
	private UserDAOImpl uDao;
	
	private User user;

	
	
	@BeforeEach
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		this.uDao = new UserDAOImpl(edb);
		when(edb.getDBConnection()).thenReturn(con);
		when(con.prepareStatement(isA(String.class))).thenReturn(ps);
		when(con.prepareCall(isA(String.class))).thenReturn(cs);
		when(ps.executeQuery()).thenReturn(rs);
		when(ps.execute()).thenReturn(true);
		when(cs.execute()).thenReturn(true);
		when(cs.getString(isA(Integer.class))).thenReturn("Test Success");
		user = new User("aers", "apass", "Andrew", "Askin", "a@ers.html", 1);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getString(2)).thenReturn(user.getUsername());
		when(rs.getString(3)).thenReturn(user.getPassword());
		when(rs.getString(4)).thenReturn(user.getFirstName());
		when(rs.getString(5)).thenReturn(user.getLastName());
		when(rs.getString(6)).thenReturn(user.getEmail());
		when(rs.getInt(7)).thenReturn(user.getRoleId());

	}

	
	@Test
	public void getUserSuccess() throws SQLException{
		User testUser = uDao.getUser("aers");
		assertEquals(user.getFirstName(), testUser.getFirstName());
		assertEquals(user.getLastName(), testUser.getLastName());
		assertEquals(user.getEmail(), testUser.getEmail());
	}
	

	
	
	
}
