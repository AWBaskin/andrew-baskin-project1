Feature: Manager Accepts or Rejects a Reimbursement
#A Manager can approve or deny any reimbursement

		Scenario: Manager approves or denies a reimbursement requests
				Given a manager is logged into the manager webpage "<username>"
				When a manager password "<password>"
				When a manager can view all reimbursement requests
				And that manager can approve or deny a reimbursement request by inputing the resolveDate "<resolveDate>"
				And that manager inputs the reimbId "<reimbId>"
				Then the reimbursement status is submitted
				
				
		Examples:
							|username||password|| resolveDate | reimbId |
							|aers||apass||01222022  |	 3		|
				
				