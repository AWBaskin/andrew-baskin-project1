Feature: Manager Logging into ERS Webpage


		Scenario Outline: Login and view all past and pending reimbursements
				Given a manager is at the home page 
				When a user of role manager inputs their username "<username>"
				And a user of role manager inputs their password "<password>"
				And a user submits their manager information
				Then the manager is redirected to the manager ers page
				
				
				
				
	Examples:
				
				| username | password |
				| aers		 | apass		|
#				| "badlogin" | "badlogin" |