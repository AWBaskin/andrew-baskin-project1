
Feature: Employee Logging into ERS Webpage
#	An employee can login to see their own reimbursements, past and pending

		Scenario Outline: Login and view their past and pending reimbursements
				Given an employee is at the home page 
				When a user of role employee inputs their username "<username>"
				And a user of role employee inputs their password "<password>"
				And a user submits their employee information
				Then the user is redirected to the employee ers page
				
				Examples:
				
				| username 	 | password 	|
				| bers		 | bpass		|
				| cers		 | cpass		|
#				| "badlogin" | "badlogin" |
				
				
				
		


