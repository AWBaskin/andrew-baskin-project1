Feature: Employee Submits a Reimbursement
#	An employee can submit a reimbursement with an amount and a reason

		Scenario: Employee can submit a reimbursement
				Given an employee is logged into the employee webpage "<username>"
				When an employee inputs their password "<password>"
				When an employee inputs the amount "<amount>"
				When an employee inputs the description "<description>"
				When an employee inputs the submitDate "<submitDate>"
				Then the reimbursement is submitted
				
				
				
		Examples:
							|username| | password| | amount | description	| submitDate |
							| bers | | bpass | | 250 |	Test in a Test | 01222022 |
						





